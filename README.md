### demo


V 1.0.3
- 修正：只有tag版本才會build

V 1.0.2
- 調整.gitlab-ci.yml
  - 修正 `IMAGE_TAG: $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_SLUG` 為     
         `IMAGE_TAG: $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_NAME`

V 1.0.1
- 調整.gitlab.ci.yml內容
  - 取消test區塊
